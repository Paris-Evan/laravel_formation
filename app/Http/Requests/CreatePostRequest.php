<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Psy\Util\Str;

class CreatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['required','min:8'],
            'slug' => ['required','min:8','regex:/^[0-9a-z\-]+$/','unique:posts'],
            'content' => ['required']

        ];
    }
    public function prepareForValidation()
    {
        $this->merge([
            'slug' => $this->input('slug') ?: \Illuminate\Support\Str::slug($this->input('title'))
        ]);

    }

}
