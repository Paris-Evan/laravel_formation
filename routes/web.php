<?php

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// lorsque j'accéde à la page racine ('/') j'appel la fonction qui renvoie la vue welcome
Route::get('/', function () {
   return view('welcome');
});
/*
$post = new Post();
$post -> title = 'un article';
$post -> sluge = 'un-article';
$post -> content = 'un contenu';
$post -> save();
return $post;
*/
Route::prefix('/blog')->name('blog.')->controller(\App\Http\Controllers\BlogController::class)->group(function(){
    Route::get('/','index')->name('index');
/*
 * modification BDD
    $post = Post::find(2);
    $post->slug = 'mon-second-article';
    $post->save();
*/
    Route::get('/new','create')->name('create');
    Route::post('/new','store');

    Route::get('/{slug}-{post}','show')->where([
        'id'=> '[0-9]+',
        'slug'=>'[a-z0-9\-]+'
    ])->name('show');


// route de la page edit     /{slug}-{id}
    /*
    Route::get('{id}/edit','edit')->where([
        'post'=> '[0-9]+',
        'slug'=>'[a-z0-9\-]+'
    ])->name('edit');
    */
/*
    Route::get('/{post:slug}','show')->where([
        'post'=>'[a-z0-9\-]+'
    ])->name('show');
*/
});



