@extends('base')

@section('title','Créer un article')

@section('content')
    <form action="" method="post" >
        @csrf
        <div>
            <input type="text" name="title" value="{{old('title','Mon titre')}}">
            @error('title')
            {{$message}}
            @enderror
        </div>
        <div>
            <textarea name="content" >{{old('content','Mon contenu')}}</textarea>
            @error('content')
            {{$message}}
            @enderror


        </div>
        <button>enregistrer</button>

    </form>

@endsection
